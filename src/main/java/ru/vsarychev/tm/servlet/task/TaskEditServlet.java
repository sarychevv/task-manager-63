package ru.vsarychev.tm.servlet.task;

import lombok.SneakyThrows;
import ru.vsarychev.tm.enumerated.Status;
import ru.vsarychev.tm.model.Task;
import ru.vsarychev.tm.repository.ProjectRepository;
import ru.vsarychev.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final String projectId = req.getParameter("projectId");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("dateStart");
        final String dateFinishValue = req.getParameter("dateFinish");

        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        task.setProjectId(projectId);

        task.setDateStart(dateStartValue.isEmpty() ? null : dateFormat.parse(dateStartValue));
        task.setDateFinish(dateFinishValue.isEmpty() ? null : dateFormat.parse(dateFinishValue));

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }
}
