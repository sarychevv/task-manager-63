package ru.vsarychev.tm.servlet.project;

import lombok.SneakyThrows;
import ru.vsarychev.tm.enumerated.Status;
import ru.vsarychev.tm.model.Project;
import ru.vsarychev.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("dateStart");
        final String dateFinishValue = req.getParameter("dateFinish");

        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);

        project.setDateStart(dateStartValue.isEmpty() ? null : dateFormat.parse(dateStartValue));
        project.setDateFinish(dateFinishValue.isEmpty() ? null : dateFormat.parse(dateFinishValue));

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }
}
